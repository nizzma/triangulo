import sys

def line(number: int):
    """Return a string corresponding to the line for number"""
    return str(number) * number

def triangle(number: int):
    """Return a string corresponding to the triangle for number"""
    if number > 9:
        raise ValueError('El número no debe ser mayor que 9')
    
    result = ''
    for i in range(1, number + 1):
        result += line(i) + '\n'
    
    return result

def main():
    if len(sys.argv) != 2:
        print("Uso: python triangulo.py <número>")
        return

    try:
        number = int(sys.argv[1])
        triangle_output = triangle(number)
        print(triangle_output)
    except ValueError:
        print("El número debe ser un entero entre 1 y 9. Usando 9 por defecto.")
        default_output = triangle(9)
        print(default_output)

if __name__ == '__main__':
    main()
